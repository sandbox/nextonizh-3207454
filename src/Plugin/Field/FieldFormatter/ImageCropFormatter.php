<?php

namespace Drupal\image_crop_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\crop\Entity\Crop;
use Drupal\crop\Entity\CropType;
use Drupal\image\ImageStyleStorageInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Render\RendererInterface;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Plugin implementation of the 'image_crop_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "image_crop_formatter",
 *   label = @Translation("Image Crop Formatter"),
 *   field_types = {
 *     "entity_reference",
 *     "image"
 *   }
 * )
 */
class ImageCropFormatter extends ImageFormatter {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Root directory where croped images stored.
   */
  private const IMAGE_CROP_DIR = 'cropped_img';

  /**
   * Constructs an ImageCropFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\image\ImageStyleStorageInterface $image_style_storage
   *   The image style entity storage handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, ImageStyleStorageInterface $image_style_storage, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   *
   * This has to be overridden because FileFormatterBase expects $item to be
   * of type \Drupal\file\Plugin\Field\FieldType\FileItem and calls
   * isDisplayed() which is not in FieldItemInterface.
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return !$item->hasNewEntity();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['crop_type' => ''] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $link_types = [
      'content' => $this->t('Content'),
      'media' => $this->t('Media item'),
    ];
    $element['image_link']['#options'] = $link_types;

    if ($crop_types_options = CropType::getCropTypeNames()) {
      $element['crop_type'] = [
        '#title' => $this->t('Crop Type'),
        '#type' => 'select',
        '#options' => $crop_types_options,
        '#default_value' => $this->getSetting('crop_type') ? $this->getSetting('crop_type') : 'crop',
        '#required' => TRUE,
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $link_types = [
      'content' => $this->t('Linked to content'),
      'media' => $this->t('Linked to media item'),
    ];
    // Display this setting only if image is linked.
    $image_link_setting = $this->getSetting('image_link');
    if (isset($link_types[$image_link_setting])) {
      $summary[] = $link_types[$image_link_setting];
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $media_items = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($media_items)) {
      return $elements;
    }

    $crop_type = $this->getSetting('crop_type');
    $image_style_setting = $this->getSetting('image_style');

    /** @var \Drupal\media\MediaInterface[] $media_items */
    foreach ($media_items as $delta => $media) {
      $image_uri = $media->get('image')->entity->uri->value;
      $crop = Crop::findCrop($image_uri, $crop_type);
      // If image has crop then create cropped image.
      if ($crop) {
        $file_system = \Drupal::service('file_system');
        // Hash that responsible for changing crop coordinates of image.
        $crop_image_hash = substr(md5(implode($crop->position()) . implode($crop->anchor())), 0, 8);
        // Hash that responsible for crop type of image.
        $crop_type_hash = substr(md5($crop_type . $file_system->basename($image_uri)), 0, 8);
        $file_name = $file_system->basename($image_uri);

        $image_style = $this->imageStyleStorage->load($image_style_setting);
        $crop_dir = 'public://' . self::IMAGE_CROP_DIR . '/' . $crop_type . '_' . $crop_type_hash;
        $crop_image_uri = $crop_dir . '/' . $crop_image_hash . '-' . $file_name;
        // Crop image if it doesnt exists.
        if (!file_exists($crop_image_uri)) {
          // Remove old crop type image.
          $file_system->deleteRecursive($crop_dir);
          $file_system->prepareDirectory($crop_dir, FileSystemInterface::CREATE_DIRECTORY);
          // Copy source image in to crop type directory.
          $file_system->copy($image_uri, $crop_image_uri, $file_system::EXISTS_REPLACE);
          // Create cropped image.
          $crop_image = \Drupal::service('image.factory')->get($crop_image_uri);
          // Recalc crop coordinates of image from center to left top point.
          $x = (int) round($crop->x->value - ($crop->width->value / 2));
          $y = (int) round($crop->y->value - ($crop->height->value / 2));
          $crop_image->crop($x, $y, $crop->width->value, $crop->height->value);
          $crop_image->save();

          // Flush image style.
          $image_style->flush($crop_image_uri);
        }

        $image_style_uri = $image_style->buildUrl($crop_image_uri);
        $image_render = [
          '#theme' => 'image',
          '#uri' => $image_style_uri,
          '#alt' => $media->thumbnail->alt,
          '#title' => $media->name->value,
        ];

        // Check if the formatter involves a link.
        if ($this->getSetting('image_link')) {
          $url = $this->getMediaThumbnailUrl($media, $items->getEntity());
          $elements[$delta] = [
            '#type' => 'link',
            '#url' => $url,
            '#title' => $image_render,
          ];
        }
        else {
          // Return only image.
          $elements[$delta] = $image_render;
        }
      }
      else {
        // If the image was not cropped.
        $elements[$delta] = [
          '#theme' => 'image_formatter',
          '#item' => $media->get('image')->first(),
          '#item_attributes' => [],
          '#image_style' => $this->getSetting('image_style'),
          '#url' => $this->getMediaThumbnailUrl($media, $items->getEntity()),
        ];
      }
      // Add cacheability of each item in the field.
      $this->renderer->addCacheableDependency($elements[$delta], $media);
    }

    // Add cacheability of the image style setting.
    if ($this->getSetting('image_link') && ($image_style = $this->imageStyleStorage->load($image_style_setting))) {
      $this->renderer->addCacheableDependency($elements, $image_style);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that reference
    // media items.
    return ($field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'media');
  }

  /**
   * Get the URL for the media thumbnail.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media item.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that the field belongs to.
   *
   * @return \Drupal\Core\Url|null
   *   The URL object for the media item or null if we don't want to add
   *   a link.
   */
  protected function getMediaThumbnailUrl(MediaInterface $media, EntityInterface $entity) {
    $url = NULL;
    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    elseif ($image_link_setting === 'media') {
      $url = $media->toUrl();
    }
    return $url;
  }

}
